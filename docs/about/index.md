# About me

![](../images/00_about_elmer.jpg)

I am a Metallurgical Engineer with extensive experience in mining projects, cost estimation, and operational control. My specialization in Biohydrometallurgy has allowed me to develop innovative metallurgical tests, such as bioleaching of sulfides and dump leaching. I hold patents in leaching technologies, including the groundbreaking Chapilix technology. My ability to plan, control, and track projects, as well as my proficiency in handling metallurgical software like LIMN, ISOCAL, MEUM, CULEACH, MODSIM, BRUNO, and HSC, makes me a valuable asset in forming innovative teams in Biotechnology between Peru and Chile. Passionate about research and development of cutting-edge metallurgical processes at Fablab.

## My family

I was born in a nice city and lived with my family since 16 years ago.

![](../images/week01/00_about_family.jpg)
![](../images/week01/00_about_family1.jpg)
