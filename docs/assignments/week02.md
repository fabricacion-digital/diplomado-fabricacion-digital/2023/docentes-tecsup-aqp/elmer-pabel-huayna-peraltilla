# 2. Project management

This week I worked on defining my final project idea and started to getting used to the documentation process.

## Research : Install editor, Git and configure enviroment

Here are the step-by-step instructions to add a project in GitLab with the specified steps:

  1.  Install Sublime Text: Download and install Sublime Text on your local machine from the official website.
![](../images/week02_01.png)
  2.  Create a GitLab Account: If you don't have one already, go to the GitLab website and sign up for a new account.
![](../images/week02_02.png)
  3.  Select Sublime Text as GitLab Editor: After logging in to GitLab, navigate to your profile settings and find the "Editor" section. Choose "Sublime Text" as your preferred editor for GitLab.
![](../images/week02_03.png)
  4.  Install Git : If you haven't already, download and install Git Bash on your local machine. This will provide you with a command-line interface to work with Git.
![](../images/week02_04.png)
  5.  Clone the Repository: Open Git Bash, navigate to the desired folder location, and run the following command to clone the repository:


git clone https://gitlab.com/fabricacion-digital/diplomado-fabricacion-digital/2023/docentes-tecsup-aqp/elmer-pabel-huayna-peraltilla.git

![](../images/week02_05.png)
6. Generate SSH Key: In Git Bash, run the following command to generate an SSH key for secure authentication:

ssh-keygen -t rsa -b 2048 -C "ehuayna@tecsup.edu.pe"

![](../images/week02_06.png)
7. Copy SSH Key to GitLab: Run the following command to copy the generated SSH key and paste it into your GitLab account settings under "SSH Keys":


cat ~/.ssh/id_rsa.pub
![](../images/week02_07.png)
8. Configure Local Username: Set up your local Git configuration with your username using the following command:


git config --global user.name "ehuayna1"

9. Configure GitLab Email: Configure your GitLab email using the following command:

git config --global user.email "ehuayna@tecsup.edu.pe"
![](../images/week02_09.png)
10. Initialize Git Repository: Inside the cloned repository folder, run the following command to initialize a new Git repository:

    git init

Now you have successfully added the project from GitLab to your local machine, configured your Git settings, and initialized a local Git repository. You can start making changes to your project and use Git commands to manage version control and collaborate with others on GitLab.


## Useful links

- [Jekyll](http://jekyll.org)
- [Google](http://google.com)
- [Markdown](https://en.wikipedia.org/wiki/Markdown)

## Code Example

Use the three backticks to separate code.

```
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}
```

## Gallery

![](../images/sample-photo.jpg)

## Video

### From Vimeo

<iframe src="https://player.vimeo.com/video/10048961" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/10048961">Sound Waves</a> from <a href="https://vimeo.com/radarboy">George Gally (Radarboy)</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

### From Youtube

<iframe width="560" height="315" src="https://www.youtube.com/embed/jjNgJFemlC4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

## 3D Models

<div class="sketchfab-embed-wrapper"><iframe width="640" height="480" src="https://sketchfab.com/models/658c8f8a2f3042c3ad7bdedd83f1c915/embed" frameborder="0" allow="autoplay; fullscreen; vr" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

<p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
    <a href="https://sketchfab.com/models/658c8f8a2f3042c3ad7bdedd83f1c915?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Dita&#39;s Gown</a>
    by <a href="https://sketchfab.com/francisbitontistudio?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Francis Bitonti Studio</a>
    on <a href="https://sketchfab.com?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a>
</p>
</div>
